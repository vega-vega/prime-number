var stompClient = null;

function connect() {
    var socket = new SockJS('/gs-guide-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/number', function (data) {
            addValue(JSON.parse(data.body));
        });
    });
}
connect();

function sendLength() {
    stompClient.send("/app/autonumber", {}, JSON.stringify({"length": parseInt($("#length").val())}));
}

function loadData() {
    $.getJSON('/primenumber', {length: $("#length").val()}, function(data) {
        addValue(data);
    });
}

function addValue(data) {
    var items = [];
    $.each(data, function(key, val) {
        items.push('<li id="' + key + '">' + val + '</li>');
    });
    $('<ul/>', {
        'class': 'my-new-list',
        html: items.join('')
    }).appendTo('#content');
}

$(function () {
    $("#auto").click(function() { sendLength(); });
});

$(window).on("beforeunload", function() {
    stompClient.send("/app/stop", {});
});