package com.vegzul.primenumber.controller;

import com.vegzul.primenumber.itrfc.GenerateNumberService;
import com.vegzul.primenumber.model.LengthModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicBoolean;

@RestController
public class GenerateNumberController {

    @Autowired
    private GenerateNumberService generateNumberService;

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    private static final AtomicBoolean atomicBoolean = new AtomicBoolean(false);

    /**
     * Return generated prime number value in json.
     */
    @RequestMapping(value = "/primenumber", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Integer[][] getPrimeNumber(@RequestParam(name="length") Integer numberLength) {
        return generateNumberService.generate(numberLength);
    }

    @MessageMapping("/autonumber")
    public void startAutoNumber(@RequestParam(name="length") LengthModel lengthModel) throws InterruptedException {
        if (lengthModel.getLength() == null) {
            return;
        }
        atomicBoolean.set(true);

        while (atomicBoolean.get()) {
            Thread.sleep(10000);
            if (atomicBoolean.get()) {
                messagingTemplate.convertAndSend("/topic/number", generateNumberService.generate(lengthModel.getLength()));
            }
        }
    }

    @MessageMapping("/stop")
    public void stopAutoNumber() {
        atomicBoolean.set(false);
    }
}
