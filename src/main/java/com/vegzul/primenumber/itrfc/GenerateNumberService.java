package com.vegzul.primenumber.itrfc;

public interface GenerateNumberService {

    Integer[][] generate(int length);
}
