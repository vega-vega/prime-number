package com.vegzul.primenumber.model;

public class LengthModel {

    private Integer length;

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }
}
