package com.vegzul.primenumber.service;

import com.vegzul.primenumber.itrfc.GenerateNumberService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class GeneratePrimeNumberService implements GenerateNumberService{

    private static final int MIN_LENGTH = 10;
    private static final int MAX_LENGTH = 100;
    private static final int THREAT_NUM = 5;
    private static final int THREAT_SIZE = 6;

    private Random random = new Random();

    public Integer[][] generate(int length) {
        length = length < MIN_LENGTH ? MIN_LENGTH : length > MAX_LENGTH ? MAX_LENGTH : length;
        Integer[][] threatArr = new Integer[THREAT_NUM][THREAT_SIZE];

        for (int i = 0; i < THREAT_NUM; i++) {
            List<Integer> generatedList = generateList(length);
            List<Integer> existChance = new ArrayList<>();
            while (true) {
                if (existChance.size() == (length - 1) || stepIsDone(threatArr, i)) {
                    break;
                }

                int position = random.nextInt(length);
                if (!existChance.contains(position)) {
                    existChance.add(position);
                    Integer value = generatedList.get(position);

                    if (!isExistValue(threatArr, value)) {
                        addValue(threatArr, value, i);
                    }
                }
            }
        }

        return threatArr;
    }

    /**
     * Add value in empty position.
     */
    private void addValue(Integer[][] threatArr, Integer value, Integer step) {
        for (int n = 0; n < THREAT_SIZE; n ++) {
            if (threatArr[step][n] == null) {
                threatArr[step][n] = value;
                break;
            }
        }
    }

    /**
     * Check, this value is exist.
     */
    private boolean isExistValue(Integer[][] threatArr, Integer value) {
        for (Integer[] intArr : threatArr) {
            for (Integer i : intArr) {
                if (value.equals(i)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * All position matter.
     */
    private boolean stepIsDone(Integer[][] threatArr, int step) {
        boolean isDone = true;
        for (int n = 0; n < THREAT_SIZE; n ++) {
            if (threatArr[step][n] == null) {
                isDone = false;
            }
        }

        return isDone;
    }

    /**
     * Generate list of prime number with length.
     */
    private List<Integer> generateList(int length) {
        List<Integer> integerList = new ArrayList<>();
        int number = 0;

        for (int i = 0; i<= length; i++) {
            if (number == 3) {
                integerList.add(number);
                number += 2;
                continue;
            }

            while (true) {
                number += 2;
                if (number == 2) {
                    integerList.add(number);
                    number = 3;
                    break;
                } else if (isPrime(number)) {
                    integerList.add(number);
                    break;
                }
            }
        }

        return integerList;
    }

    /**
     * Check, is valid prime number.
     */
    private boolean isPrime(int number) {
        int k = 3;
        while (k < Math.sqrt(number) + 1) {
            if (number%k == 0) return false;
            k += 2;
        }

        return true;
    }
}
